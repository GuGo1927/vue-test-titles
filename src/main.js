import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./stores/store";

Vue.config.productionTip = false;
require("./plugins/setup.plugins");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
