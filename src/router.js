import Vue from "vue";
import Router from "vue-router";
import TitleList from "./components/TitleList";
import CurrentTitle from "./components/CurrentTitle";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "title-list",
      component: TitleList
    },
    {
      path: "/:id",
      component: CurrentTitle
    }
  ]
});
