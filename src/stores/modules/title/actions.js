import * as types from "./types";
import * as mutation_types from "./mutation_types";
import { GetTitles } from "./api/methods";

export default {
  [types.A_GET_TITLES](context) {
    GetTitles()
      .then(response => {
        context.commit(mutation_types.M_GET_TITLES, response);
      })
      .catch(error => {
        console.log(error);
      });
  },
  [types.A_ADD_COMMENT](context, payload) {
    context.commit(mutation_types.M_ADD_COMMENT, payload);
  },
  [types.A_DELETE_COMMENT](context, payload) {
    context.commit(mutation_types.M_DELETE_COMMENT, payload);
  }
};
