import * as mutation_types from "./mutation_types";

export default {
  [mutation_types.M_GET_TITLES]: (state, payload) => {
    state.titlesList = payload.data;
  },
  [mutation_types.M_ADD_COMMENT]: (state, payload) => {
    state.comments.push(payload);
  },
  [mutation_types.M_DELETE_COMMENT]: (state, payload) => {
    state.comments.splice(
      state.comments.findIndex((item, key) => {
        return key === payload.key;
      }),
      1
    );
  }
};
