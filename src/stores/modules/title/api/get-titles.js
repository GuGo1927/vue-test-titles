import Vue from "vue";

export default () => {
  return Vue.Api.Get("https://jsonplaceholder.typicode.com/posts");
};
